// console.log("Hello World")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function personInfo(){
		let fullName = prompt("Enter your Full Name: ");
		let age = prompt("Enter your age: ");
		let location = prompt("Enter your location: ");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.")
		console.log("You live in " + location + " City.")

	}

	personInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favoriteArtists(){
		console.log("1. Taylor Swift");
		console.log("2. The Beatles");
		console.log("3. Bee Gees");
		console.log("4. Imagine Dragons");
		console.log("5. Carpenters");

	}
	
	favoriteArtists();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

		let rottenTomatoes="Rotten Tomatoes Rating: "

		function favoriteMovies(){
		console.log("1. Me Before You");
		console.log(rottenTomatoes + "54%")
		console.log("2. Ratatouille");
		console.log(rottenTomatoes + "96%")
		console.log("3. Bolt");
		console.log(rottenTomatoes + "89%")
		console.log("4. Corpse Bride");
		console.log(rottenTomatoes + "84%")
		console.log("5. Frozen");
		console.log(rottenTomatoes + "90%")
	}
	
	favoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends= function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
